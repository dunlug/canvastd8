#include "CppUnitTest.h"
#include "element.h"
#include "liste_chainee.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

#ifdef LISTE_CHAINEE
namespace TestListeChainee
{
	TEST_CLASS(Ajouter)
	{
	public:
		TEST_METHOD(ajouter)
		{
			ListeChainee *liste = new ListeChainee();
			liste->ajouter(0);
			liste->ajouter(1);
			liste->ajouter(2);
			Assert::AreEqual(0, (*liste)[0]);
			Assert::AreEqual(1, (*liste)[1]);
			Assert::AreEqual(2, (*liste)[2]);

			Assert::IsTrue(liste->check_integrity(), L"Integrity check failed");
			delete liste;
			Assert::AreEqual(0, Element::get_nb_instances(), L"Memory leak");
		}

	};

	TEST_CLASS(Inserer)
	{
		TEST_METHOD(inserer_debut)
		{
			ListeChainee* liste = new ListeChainee();
			for (int i = 0; i < 10; i++)
			{
				liste->ajouter(i);
			}

			// Insertion d'un élément au début
			liste->inserer(10, 0);
			Assert::AreEqual(10, (*liste)[0]);
			Assert::AreEqual(0, (*liste)[1]);
			Assert::AreEqual(11, (*liste).taille());

			Assert::IsTrue(liste->check_integrity(), L"Integrity check failed");
			delete liste;
			Assert::AreEqual(0, Element::get_nb_instances(), L"Memory leak");
		}

		TEST_METHOD(inserer_milieu)
		{
			ListeChainee* liste = new ListeChainee;
			for (int i = 0; i < 10; i++)
			{
				(*liste).ajouter(i);
			}

			// Insertion d'un élément au milieu
			(*liste).inserer(10, 5);
			Assert::AreEqual(4, (*liste)[4]);
			Assert::AreEqual(10, (*liste)[5]);
			Assert::AreEqual(5, (*liste)[6]);
			Assert::AreEqual(11, (*liste).taille());

			Assert::IsTrue(liste->check_integrity(), L"Integrity check failed");
			delete liste;
			Assert::AreEqual(0, Element::get_nb_instances(), L"Memory leak");
		}

		TEST_METHOD(inserer_fin)
		{
			ListeChainee* liste = new ListeChainee;
			for (int i = 0; i < 10; i++)
			{
				(*liste).ajouter(i);
			}

			// Insertion d'un élément à la fin
			(*liste).inserer(10, 10);
			Assert::AreEqual(10, (*liste)[10]);
			Assert::AreEqual(9, (*liste)[9]);
			Assert::AreEqual(11, (*liste).taille());

			Assert::IsTrue(liste->check_integrity(), L"Integrity check failed");
			delete liste;
			Assert::AreEqual(0, Element::get_nb_instances(), L"Memory leak");
		}

		TEST_METHOD(inserer_empty)
		{
			ListeChainee* liste = new ListeChainee;
			(*liste).inserer(5, 0);
			Assert::AreEqual(5, (*liste)[0]);
			Assert::AreEqual(1, (*liste).taille());

			Assert::IsTrue(liste->check_integrity(), L"Integrity check failed");
			delete liste;
			Assert::AreEqual(0, Element::get_nb_instances(), L"Memory leak");
		}

		TEST_METHOD(inserer_empty_throw)
		{
			ListeChainee* liste = new ListeChainee;
			bool ok = false;
			try
			{
				(*liste).inserer(3, 1);
			}
			catch (std::out_of_range)
			{
				ok = true;
			}
			catch (...)
			{
				Assert::Fail(L"Not the correct exception type");
			}
			if (!ok)
				Assert::Fail(L"No exception");

			Assert::IsTrue(liste->check_integrity(), L"Integrity check failed");
			delete liste;
			Assert::AreEqual(0, Element::get_nb_instances(), L"Memory leak");
		}
	};

	TEST_CLASS(Supprimer)
	{
		TEST_METHOD(supprimer_debut)
		{
			ListeChainee* liste = new ListeChainee;
			for (int i = 0; i < 10; i++)
			{
				(*liste).ajouter(i);
			}

			(*liste).supprimer(0);
			Assert::AreEqual(1, (*liste)[0]);
			Assert::AreEqual(9, (*liste).taille());

			Assert::IsTrue(liste->check_integrity(), L"Integrity check failed");
			delete liste;
			Assert::AreEqual(0, Element::get_nb_instances(), L"Memory leak");
		}

		TEST_METHOD(supprimer_milieu)
		{
			ListeChainee* liste = new ListeChainee;
			for (int i = 0; i < 10; i++)
			{
				(*liste).ajouter(i);
			}

			(*liste).supprimer(5);
			Assert::AreEqual(4, (*liste)[4]);
			Assert::AreEqual(6, (*liste)[5]);
			Assert::AreEqual(9, (*liste).taille());

			Assert::IsTrue(liste->check_integrity(), L"Integrity check failed");
			delete liste;
			Assert::AreEqual(0, Element::get_nb_instances(), L"Memory leak");
		}

		TEST_METHOD(supprimer_fin)
		{
			ListeChainee* liste = new ListeChainee;
			for (int i = 0; i < 10; i++)
			{
				(*liste).ajouter(i);
			}

			(*liste).supprimer(9);
			Assert::AreEqual(8, (*liste)[8]);
			Assert::AreEqual(9, (*liste).taille());

			Assert::IsTrue(liste->check_integrity(), L"Integrity check failed");
			delete liste;
			Assert::AreEqual(0, Element::get_nb_instances(), L"Memory leak");
		}

		TEST_METHOD(supprimer_unique)
		{
			ListeChainee* liste = new ListeChainee;
			(*liste).ajouter(3);
			(*liste).supprimer(0);
			Assert::AreEqual(0, (*liste).taille());

			Assert::IsTrue(liste->check_integrity(), L"Integrity check failed");
			delete liste;
			Assert::AreEqual(0, Element::get_nb_instances(), L"Memory leak");
		}

		TEST_METHOD(supprimer_deux_deb)
		{
			ListeChainee* liste = new ListeChainee;
			(*liste).ajouter(3);
			(*liste).ajouter(5);
			(*liste).supprimer(0);
			Assert::AreEqual(5, (*liste)[0]);
			Assert::AreEqual(1, (*liste).taille());

			Assert::IsTrue(liste->check_integrity(), L"Integrity check failed");
			delete liste;
			Assert::AreEqual(0, Element::get_nb_instances(), L"Memory leak");
		}

		TEST_METHOD(supprimer_deux_fin)
		{
			ListeChainee* liste = new ListeChainee;
			(*liste).ajouter(3);
			(*liste).ajouter(5);
			(*liste).supprimer(1);
			Assert::AreEqual(3, (*liste)[0]);
			Assert::AreEqual(1, (*liste).taille());

			Assert::IsTrue(liste->check_integrity(), L"Integrity check failed");
			delete liste;
			Assert::AreEqual(0, Element::get_nb_instances(), L"Memory leak");
		}

		TEST_METHOD(supprimer_empty_throw)
		{
			ListeChainee* liste = new ListeChainee;
			bool ok = false;
			try
			{
				(*liste).supprimer(0);
			}
			catch (std::out_of_range)
			{
				ok = true;
			}
			catch (...)
			{
				Assert::Fail(L"Not the good type of exception, should be std::out_of_range");
			}
			if (!ok)
			{
				Assert::Fail(L"No exception");
			}

			Assert::IsTrue(liste->check_integrity(), L"Integrity check failed");
			delete liste;
			Assert::AreEqual(0, Element::get_nb_instances(), L"Memory leak");
		}

		TEST_METHOD(supprimer_neg_throw)
		{
			ListeChainee* liste = new ListeChainee;
			bool ok = false;
			for (int i = 0; i < 10; i++)
			{
				(*liste).ajouter(i);
			}
			try
			{
				(*liste).supprimer(-1);
			}
			catch (std::out_of_range)
			{
				ok = true;
			}
			catch (...)
			{
				Assert::Fail(L"Not the good type of exception, should be std::out_of_range");
			}
			if (!ok)
			{
				Assert::Fail(L"No exception");
			}

			Assert::IsTrue(liste->check_integrity(), L"Integrity check failed");
			delete liste;
			Assert::AreEqual(0, Element::get_nb_instances(), L"Memory leak");
		}

		TEST_METHOD(supprimer_high_throw)
		{
			ListeChainee* liste = new ListeChainee;
			bool ok = false;
			for (int i = 0; i < 10; i++)
			{
				(*liste).ajouter(i);
			}

			try
			{
				(*liste).supprimer(20);
			}
			catch (std::out_of_range)
			{
				ok = true;
			}
			catch (...)
			{
				Assert::Fail(L"Not the good type of exception, should be std::out_of_range");
			}
			if (!ok)
			{
				Assert::Fail(L"No exception");
			}

			Assert::IsTrue(liste->check_integrity(), L"Integrity check failed");
			delete liste;
			Assert::AreEqual(0, Element::get_nb_instances(), L"Memory leak");
		}
	};

	TEST_CLASS(Operator_at)
	{
		TEST_METHOD(operator_at)
		{
			ListeChainee* liste = new ListeChainee;
			// Accès lecture
			for (int i = 0; i < 10; i++)
			{
				(*liste).ajouter(i);
				Assert::AreEqual((*liste)[i], i);
			}

			// Accès écriture
			for (int i = 0; i < 10; i++)
			{
				(*liste)[i] = (*liste)[i] * 2;
				Assert::AreEqual(i * 2, (*liste)[i]);
			}

			Assert::IsTrue(liste->check_integrity(), L"Integrity check failed");
			delete liste;
			Assert::AreEqual(0, Element::get_nb_instances(), L"Memory leak");
		}

		TEST_METHOD(operator_at_neg_throw)
		{
			ListeChainee* liste = new ListeChainee;
			(*liste).ajouter(3);
			(*liste).ajouter(2);
			(*liste).ajouter(7);

			bool ok = false;
			// Accès negatif
			try
			{
				(*liste)[-1];
			}
			catch (std::out_of_range)
			{
				ok = true;
			}
			catch (...)
			{
				Assert::Fail(L"Negative access doesn't throw std::out_of_range exception");
			}
			if (!ok)
			{
				Assert::Fail(L"No exception");
			}

			Assert::IsTrue(liste->check_integrity(), L"Integrity check failed");
			delete liste;
			Assert::AreEqual(0, Element::get_nb_instances(), L"Memory leak");
		}

		TEST_METHOD(operator_at_high_throw)
		{
			ListeChainee* liste = new ListeChainee;
			(*liste).ajouter(4);
			(*liste).ajouter(8);

			bool ok = false;
			// Accès hors bornes
			try
			{
				(*liste)[20];
			}
			catch (std::out_of_range)
			{
				ok = true;
			}
			catch (...)
			{
				Assert::Fail(L"Out of bounds access doesn't throw std::out_of_range exception");
			}
			if (!ok)
			{
				Assert::Fail(L"No exception");
			}

			Assert::IsTrue(liste->check_integrity(), L"Integrity check failed");
			delete liste;
			Assert::AreEqual(0, Element::get_nb_instances(), L"Memory leak");
		}

		TEST_METHOD(operator_at_empty_throw)
		{
			// Liste vide
			ListeChainee* liste = new ListeChainee;
			bool ok = false;
			try
			{
				(*liste)[0];
			}
			catch (std::out_of_range)
			{
				ok = true;
			}
			catch (...)
			{
				Assert::Fail(L"Access to empty list doesn't throw std::out_of_range exception");
			}
			if (!ok)
			{
				Assert::Fail(L"No exception");
			}

			Assert::IsTrue(liste->check_integrity(), L"Integrity check failed");
			delete liste;
			Assert::AreEqual(0, Element::get_nb_instances(), L"Memory leak");
		}

		TEST_METHOD(operator_at_end_throw)
		{
			ListeChainee* liste = new ListeChainee;
			bool ok = false;
			(*liste).ajouter(4);
			(*liste).ajouter(8);
			(*liste).ajouter(2);
			try
			{
				(*liste)[3];
			}
			catch (std::out_of_range)
			{
				ok = true;
			}
			catch (...)
			{
				Assert::Fail(L"Access to liste[liste.taille()] doesn't throw std::out_of_range exception");
			}
			if (!ok)
			{
				Assert::Fail(L"No exception");
			}

			Assert::IsTrue(liste->check_integrity(), L"Integrity check failed");
			delete liste;
			Assert::AreEqual(0, Element::get_nb_instances(), L"Memory leak");
		}
	};

	TEST_CLASS(Taille)
	{
		TEST_METHOD(taille)
		{
			ListeChainee* liste = new ListeChainee;
			for (int i = 0; i < 10; i++)
			{
				Assert::AreEqual(i, (*liste).taille());
				(*liste).ajouter(10);
			}

			Assert::IsTrue(liste->check_integrity(), L"Integrity check failed");
			delete liste;
			Assert::AreEqual(0, Element::get_nb_instances(), L"Memory leak");
		}
	};
}
#endif