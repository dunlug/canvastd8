#pragma once

class Forme
{
public:
	virtual void draw() = 0;
};