﻿#include "CppUnitTest.h"
#include "element.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
#ifdef ELEMENT
namespace TestElement
{
	TEST_CLASS(Read_access)
	{
	public:
		TEST_METHOD(getValeur)
		{
			Element e1(0, nullptr, nullptr);
			Assert::AreEqual(0, e1.getValeur());
			Element e2(1, nullptr, nullptr);
			Assert::AreEqual(1, e2.getValeur());
		}

		TEST_METHOD(getPrecedent)
		{
			Element prec(2, nullptr, nullptr);
			Element cur(3, &prec, nullptr);
			Assert::AreEqual((int)nullptr, (int)prec.getPrecedent());
			Assert::AreEqual((int)&prec, (int)cur.getPrecedent());
		}
		TEST_METHOD(getSuivant)
		{
			Element suiv(4, nullptr, nullptr);
			Element cur(5, nullptr, &suiv);
			Assert::AreEqual((int)nullptr, (int)suiv.getSuivant());
			Assert::AreEqual((int)&suiv, (int)cur.getSuivant());
		}
	};

	TEST_CLASS(Write_access)
	{
		TEST_METHOD(setPrecedent)
		{
			Element prec(6, nullptr, nullptr);
			Element cur(7, nullptr, nullptr);
			cur.setPrecedent(&prec);
			Assert::AreEqual((int)cur.getPrecedent(), (int)&prec);
		}
		TEST_METHOD(setSuivant)
		{
			Element suiv(8, nullptr, nullptr);
			Element cur(9, nullptr, nullptr);
			cur.setSuivant(&suiv);
			Assert::AreEqual((int)cur.getSuivant(), (int)&suiv);
		}
	};
}
#endif