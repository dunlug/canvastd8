﻿#include "CppUnitTest.h"
#include "element.h"
#include "forme.h"
#include "triangle.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

#ifdef TEMPLATE_ELEMENT
namespace TestTemplateElementInt
{
	TEST_CLASS(Read_access)
	{
	public:
		TEST_METHOD(getValeur)
		{
			Element<int> e1(0, nullptr, nullptr);
			Assert::AreEqual(0, e1.getValeur());
			Element<int> e2(1, nullptr, nullptr);
			Assert::AreEqual(1, e2.getValeur());
		}

		TEST_METHOD(getPrecedent)
		{
			Element<int> prec(2, nullptr, nullptr);
			Element<int> cur(3, &prec, nullptr);
			Assert::AreEqual((int)nullptr, (int)prec.getPrecedent());
			Assert::AreEqual((int)&prec, (int)cur.getPrecedent());
		}
		TEST_METHOD(getSuivant)
		{
			Element<int> suiv(4, nullptr, nullptr);
			Element<int> cur(5, nullptr, &suiv);
			Assert::AreEqual((int)nullptr, (int)suiv.getSuivant());
			Assert::AreEqual((int)&suiv, (int)cur.getSuivant());
		}
	};
	TEST_CLASS(Write_access)
	{
		TEST_METHOD(setPrecedent)
		{
			Element<int> prec(6, nullptr, nullptr);
			Element<int> cur(7, nullptr, nullptr);
			cur.setPrecedent(&prec);
			Assert::AreEqual((int)cur.getPrecedent(), (int)&prec);
		}
		TEST_METHOD(setSuivant)
		{
			Element<int> suiv(8, nullptr, nullptr);
			Element<int> cur(9, nullptr, nullptr);
			cur.setSuivant(&suiv);
			Assert::AreEqual((int)cur.getSuivant(), (int)&suiv);
		}
	};
}

namespace TestTemplateElementString
{
	TEST_CLASS(Read_access)
	{
	public:
		TEST_METHOD(getValeur)
		{
			Element<std::string> e1("lundi", nullptr, nullptr);
			Assert::AreEqual("lundi", e1.getValeur());
			Element<std::string> e2("mardi", nullptr, nullptr);
			Assert::AreEqual("mardi", e2.getValeur());
		}

		TEST_METHOD(getPrecedent)
		{
			Element<std::string> prec("mercredi", nullptr, nullptr);
			Element<std::string> cur("jeudi", &prec, nullptr);
			Assert::AreEqual((int)nullptr, (int)prec.getPrecedent());
			Assert::AreEqual((int)&prec, (int)cur.getPrecedent());
		}
		
		TEST_METHOD(getSuivant)
		{
			Element<std::string> suiv("vendredi", nullptr, nullptr);
			Element<std::string> cur("samedi", nullptr, &suiv);
			Assert::AreEqual((int)nullptr, (int)suiv.getSuivant());
			Assert::AreEqual((int)&suiv, (int)cur.getSuivant());
		}
	};

	TEST_CLASS(Write_access)
	{
		TEST_METHOD(setPrecedent)
		{
			Element<std::string> prec("dimanche", nullptr, nullptr);
			Element<std::string> cur("ABC", nullptr, nullptr);
			cur.setPrecedent(&prec);
			Assert::AreEqual((int)cur.getPrecedent(), (int)&prec);
		}

		TEST_METHOD(setSuivant)
		{
			Element<std::string> suiv("DEF", nullptr, nullptr);
			Element<std::string> cur("XYZ", nullptr, nullptr);
			cur.setSuivant(&suiv);
			Assert::AreEqual((int)cur.getSuivant(), (int)&suiv);
		}
	};
}

namespace TestTemplateElementForme
{
	TEST_CLASS(Read_access)
	{
	public:
		TEST_METHOD(getValeur)
		{
			Triangle t1, t2;
			Element<Forme*> e1(&t1, nullptr, nullptr);
			Assert::AreEqual(&t1, e1.getValeur());
			Element<Forme*> e2(&t2, nullptr, nullptr);
			Assert::AreEqual(&t2, e2.getValeur());
		}

		TEST_METHOD(getPrecedent)
		{
			Element<Forme*> prec(nullptr, nullptr, nullptr);
			Element<Forme*> cur(nullptr, &prec, nullptr);
			Assert::AreEqual((int)nullptr, (int)prec.getPrecedent());
			Assert::AreEqual((int)&prec, (int)cur.getPrecedent());
		}

		TEST_METHOD(getSuivant)
		{
			Element<Forme*> suiv(nullptr, nullptr, nullptr);
			Element<Forme*> cur(nullptr, nullptr, &suiv);
			Assert::AreEqual((int)nullptr, (int)suiv.getSuivant());
			Assert::AreEqual((int)&suiv, (int)cur.getSuivant());
		}
	};

	TEST_CLASS(Write_access)
	{
		TEST_METHOD(setPrecedent)
		{
			Element<Forme*> prec(nullptr, nullptr, nullptr);
			Element<Forme*> cur(nullptr, nullptr, nullptr);
			cur.setPrecedent(&prec);
			Assert::AreEqual((int)cur.getPrecedent(), (int)&prec);
		}

		TEST_METHOD(setSuivant)
		{
			Element<Forme*> suiv(nullptr, nullptr, nullptr);
			Element<Forme*> cur(nullptr, nullptr, nullptr);
			cur.setSuivant(&suiv);
			Assert::AreEqual((int)cur.getSuivant(), (int)&suiv);
		}
	};
}
#endif