#pragma once
//#define ELEMENT
//#define TEMPLATE_ELEMENT

class Element
{
private:
	static int nb_instances;
public:
	Element();

	static int get_nb_instances() { return nb_instances; }
	~Element();
};

