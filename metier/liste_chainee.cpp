#include "liste_chainee.h"
#include "element.h"
#include <vector>


#ifdef LISTE_CHAINEE
// Function used by unit tests to check that the list structure match conception
bool ListeChainee::check_integrity() const
{
	if (!premier || !dernier)
	{
		if (!premier && !dernier) // both are null -> empty list
			return true;
		else
			return false;
	}
	else
	{
		bool ok = true;
		// Check first and last elements
		ok &= premier->getPrecedent() == nullptr;
		ok &= dernier->getSuivant() == nullptr;

		std::vector<Element*> list;
		for (Element* e = premier; e != dernier && e; e = e->getSuivant())
		{
			ok &= e != nullptr; // Can't be nullptr
			list.push_back(e);
		}

		list.push_back(dernier);

		// Check that it's ok in reverse order
		for (Element* e = dernier; e != premier; e = e->getPrecedent())
		{
			ok &= e == list.back();
			list.pop_back();
		}
		return ok;
	}
}
#endif